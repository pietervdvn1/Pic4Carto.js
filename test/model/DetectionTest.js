/*
 * Test script for model/Detection.js
 */

const assert = require('assert');
const LatLng = require("../../src/model/LatLng");
const Detection = require("../../src/model/Detection");

describe("Model > Detection", function() {
	describe("Constructor", function() {
		it("handles valid parameters", function() {
			const d1 = new Detection(Detection.SIGN_STOP, new LatLng(47.3, -1.8), 123456, "mapillary");
			assert.ok(d1 !== null);
			assert.equal(d1.type, Detection.SIGN_STOP);
			assert.equal(d1.coordinates.lat, 47.3);
			assert.equal(d1.coordinates.lng, -1.8);
			assert.equal(d1.date, 123456);
			assert.equal(d1.provider, "mapillary");
		});
		
		it("fails if missing params", () => {
			assert.throws(
				function() { 
					const d1 = new Detection();
				},
				Error,
				"model.detection.invalid.type"
			);
			
			assert.throws(
				function() { 
					const d1 = new Detection(Detection.SIGN_STOP);
				},
				Error,
				"model.detection.invalid.coordinates"
			);
			
			assert.throws(
				function() { 
					const d1 = new Detection(Detection.SIGN_STOP, new LatLng(47.3, -1.8));
				},
				Error,
				"model.detection.invalid.date"
			);
			
			assert.throws(
				function() { 
					const d1 = new Detection(Detection.SIGN_STOP, new LatLng(47.3, -1.8), 123456);
				},
				Error,
				"model.detection.invalid.provider"
			);
		});
	});
});
