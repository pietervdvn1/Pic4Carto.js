/*
 * Test script for model/LatLng.js
 */

const assert = require('assert');
const LatLng = require("../../src/model/LatLng");
const LatLngBounds = require("../../src/model/LatLngBounds");
const that = {};

describe("Model > LatLngBounds", function() {
	beforeEach(() => {
		that.a = new LatLngBounds(
			new LatLng(14, 12),
			new LatLng(30, 40));
		that.c = new LatLngBounds();
	});

	it('instantiates either passing two latlngs or an array of latlngs', () => {
		var b = new LatLngBounds([
			new LatLng(14, 12),
			new LatLng(30, 40)
		]);
		assert.ok(b.equals(that.a));
		assert.ok(b.getNorthWest().equals(new LatLng(30, 12)));
	});

	it('returns an empty bounds when not argument is given', () => {
		var bounds = new LatLngBounds();
		assert.ok(bounds instanceof LatLngBounds);
	});

	it('extends the bounds by a given point', () => {
		that.a.extend(new LatLng(20, 50));
		assert.ok(that.a.getNorthEast().equals(new LatLng(30, 50)));
	});

	it('extends the bounds by undefined', () => {
		assert.ok(that.a.extend().equals(that.a));
	});

	it('extends the bounds by raw object', () => {
		that.a.extend({lat: 20, lng: 50});
		assert.ok(that.a.getNorthEast().equals(new LatLng(30, 50)));
	});

	it('extend the bounds by an empty bounds object', () => {
		assert.ok(that.a.extend(new LatLngBounds()).equals(that.a));
	});

	it('returns the bounds center', () => {
		assert.ok(that.a.getCenter().equals(new LatLng(22, 26)));
	});

	it('pads the bounds by a given ratio', () => {
		var b = that.a.pad(0.5);

		assert.ok(b.equals(new LatLngBounds([[6, -2], [38, 54]])));
	});

	it('returns true if bounds equal', () => {
		assert.ok(that.a.equals([[14, 12], [30, 40]]));
		assert.ok(!that.a.equals([[14, 13], [30, 40]]));
		assert.ok(!that.a.equals(null));
	});

	it('returns true if properly set up', () => {
		assert.ok(that.a.isValid());
	});
	it('returns false if is invalid', () => {
		assert.ok(!that.c.isValid());
	});
	it('returns true if extended', () => {
		that.c.extend([0, 0]);
		assert.ok(that.c.isValid());
	});

	it('returns a proper bbox west value', () => {
		assert.equal(that.a.getWest(), 12);
	});

	it('returns a proper bbox south value', () => {
		assert.equal(that.a.getSouth(), 14);
	});

	it('returns a proper bbox east value', () => {
		assert.equal(that.a.getEast(), 40);
	});

	it('returns a proper bbox north value', () => {
		assert.equal(that.a.getNorth(), 30);
	});

	it('returns a proper left,bottom,right,top bbox', () => {
		assert.equal(that.a.toBBoxString(), "12,14,40,30");
	});

	it('returns a proper north-west LatLng', () => {
		assert.ok(that.a.getNorthWest().equals(new LatLng(that.a.getNorth(), that.a.getWest())));
	});

	it('returns a proper south-east LatLng', () => {
		assert.ok(that.a.getSouthEast().equals(new LatLng(that.a.getSouth(), that.a.getEast())));
	});

	it('returns true if contains latlng point as array', () => {
		assert.ok(that.a.contains([16, 20]));
		assert.ok(!new LatLngBounds(that.a).contains([5, 20]));
	});

	it('returns true if contains latlng point as {lat:, lng:} object', () => {
		assert.ok(that.a.contains({lat: 16, lng: 20}));
		assert.ok(!new LatLngBounds(that.a).contains({lat: 5, lng: 20}));
	});

	it('returns true if contains latlng point as LatLng instance', () => {
		assert.ok(that.a.contains(new LatLng([16, 20])));
		assert.ok(!new LatLngBounds(that.a).contains(new LatLng([5, 20])));
	});

	it('returns true if contains bounds', () => {
		assert.ok(that.a.contains([[16, 20], [20, 40]]));
		assert.ok(!that.a.contains([[16, 50], [8, 40]]));
	});

	it('returns true if intersects the given bounds', () => {
		assert.ok(that.a.intersects([[16, 20], [50, 60]]));
		assert.ok(!that.a.contains([[40, 50], [50, 60]]));
	});

	it('returns true if just touches the boundary of the given bounds', () => {
		assert.ok(that.a.intersects([[25, 40], [55, 50]]));
	});

	it('returns true if overlaps the given bounds', () => {
		assert.ok(that.a.overlaps([[16, 20], [50, 60]]));
	});
	it('returns false if just touches the boundary of the given bounds', () => {
		assert.ok(!that.a.overlaps([[25, 40], [55, 50]]));
	});
});
