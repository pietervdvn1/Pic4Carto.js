/*
 * Test script for model/Picture.js
 */

const assert = require('assert');
const LatLng = require("../../src/model/LatLng");
const LatLngBounds = require("../../src/model/LatLngBounds");
const Picture = require("../../src/model/Picture");

describe("Model > Picture", function() {
	describe("Constructor", function() {
		it("handles valid parameters", function() {
			const p1 = new Picture("http://test.net/img.jpg", 123456789, new LatLng(10.1, 48.7), "Myself", "Me", "My license", "http://test.net/img_details.html", 175, { image: "https://image.net" },"http://test.net/img_small.jpg", { isSpherical: true });
			
			//Check attributes
			assert.equal(p1.pictureUrl, "http://test.net/img.jpg");
			assert.equal(p1.date, 123456789);
			assert.ok(p1.coordinates.equals(new LatLng(10.1, 48.7)));
			assert.equal(p1.provider, "Myself");
			assert.equal(p1.author, "Me");
			assert.equal(p1.license, "My license");
			assert.equal(p1.detailsUrl, "http://test.net/img_details.html");
			assert.equal(p1.direction, 175);
			assert.equal(p1.osmTags.image, "https://image.net");
			assert.equal(p1.thumbUrl, "http://test.net/img_small.jpg");
			assert.ok(p1.details.isSpherical);
		});

		it("handles valid parameters (only mandatory ones)", function() {
			const p1 = new Picture("http://test.net/img.jpg", 123456789, new LatLng(10.1, 48.7), "Myself");
			
			//Check attributes
			assert.equal(p1.pictureUrl, "http://test.net/img.jpg");
			assert.equal(p1.date, 123456789);
			assert.ok(p1.coordinates.equals(new LatLng(10.1, 48.7)));
			assert.equal(p1.provider, "Myself");
			assert.equal(p1.author, null);
			assert.equal(p1.license, null);
			assert.equal(p1.detailsUrl, null);
			assert.equal(p1.direction, null);
			assert.equal(p1.osmTags, null);
			assert.equal(p1.thumbUrl, null);
			assert.equal(Object.keys(p1.details).length, 0);
		});

		it("handles missing mandatory parameters", function() {
			assert.throws(
				function() { 
					const p1 = new Picture();
				},
				Error,
				"model.picture.invalid.pictureUrl"
			);
			
			assert.throws(
				function() { 
					const p1 = new Picture("http://test.net/img.jpg");
				},
				Error,
				"model.picture.invalid.date"
			);
			
			assert.throws(
				function() { 
					const p1 = new Picture("http://test.net/img.jpg", 123456789);
				},
				Error,
				"model.picture.invalid.coords"
			);
			
			assert.throws(
				function() { 
					const p1 = new Picture("http://test.net/img.jpg", 123456789, new LatLng(10, 20));
				},
				Error,
				"model.picture.invalid.provider"
			);
		});

		it("Constructor with invalid parameters types", function() {
			assert.throws(
				function() { 
					const p1 = new Picture(123456, 123456789, new LatLng(10.1, 48.7), "Myself", "Me", "My license", "http://test.net/img_details.html", 175);
				},
				Error,
				"model.picture.invalid.pictureUrl"
			);
			
			assert.throws(
				function() { 
					const p1 = new Picture("http://test.net/img.jpg", "lol", new LatLng(10.1, 48.7), "Myself", "Me", "My license", "http://test.net/img_details.html", 175);
				},
				Error,
				"model.picture.invalid.date"
			);
			
			assert.throws(
				function() { 
					const p1 = new Picture("http://test.net/img.jpg", 123456789, 42, "Myself", "Me", "My license", "http://test.net/img_details.html", 175);
				},
				Error,
				"model.picture.invalid.coords"
			);
			
			assert.throws(
				function() { 
					const p1 = new Picture("http://test.net/img.jpg", 123456789, new LatLng(10.1, 48.7), 42, "Me", "My license", "http://test.net/img_details.html", 175);
				},
				Error,
				"model.picture.invalid.provider"
			);
			
			const p1 = new Picture("http://test.net/img.jpg", 123456789, new LatLng(10.1, 48.7), "Myself", "Me", "My license", "http://test.net/img_details.html", "north");
			assert.equal(p1.direction, null);
		});

		it("Constructor with negative timestamp", function() {
			const p1 = new Picture("http://test.net/img.jpg", -123456789, new LatLng(10.1, 48.7), "Myself");
			
			//Check attributes
			assert.equal(p1.pictureUrl, "http://test.net/img.jpg");
			assert.equal(p1.date, -123456789);
			assert.ok(p1.coordinates.equals(new LatLng(10.1, 48.7)));
			assert.equal(p1.provider, "Myself");
			assert.equal(p1.author, null);
			assert.equal(p1.license, null);
			assert.equal(p1.detailsUrl, null);
			assert.equal(p1.direction, null);
		});
	});

	describe("lookAlike", function() {
		it("lookAlike with similar pictures", function() {
			const p1 = new Picture("http://provider1.net/img1.jpg", 123456, new LatLng(1, 2), "Provider1", "Author1", "License1", "http://provider1.net/details/img1", 200);
			const p2 = new Picture("http://provider2.net/img2048.jpg", 123456, new LatLng(1, 2), "Provider2", "Author2", "License2", "http://provider2.net/see/img2048", 200);
			
			assert.ok(p1.lookAlike(p2));
		});

		it("lookAlike with distinct pictures", function() {
			const p1 = new Picture("http://provider1.net/img1.jpg", 123789, new LatLng(2, 2), "Provider1", "Author1", "License1", "http://provider1.net/details/img1", 125);
			const p2 = new Picture("http://provider2.net/img2048.jpg", 123456, new LatLng(3, 4), "Provider2", "Author2", "License2", "http://provider2.net/see/img2048", 200);
			
			assert.ok(!p1.lookAlike(p2));
		});
	});
});
