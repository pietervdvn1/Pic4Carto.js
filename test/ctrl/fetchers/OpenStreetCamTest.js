/*
 * Test script for ctrl/fetchers/OpenStreetCam.js
 */

global.XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
global.XMLHttpRequest.DONE = 4;

const assert = require('assert');
const LatLng = require("../../../src/model/LatLng");
const LatLngBounds = require("../../../src/model/LatLngBounds");
const OpenStreetCam = require("../../../src/ctrl/fetchers/OpenStreetCam");

const REQUEST_TIMEOUT = 30000;

describe("Ctrl > Fetchers > OpenStreetCam", function() {
	describe("Constructor", function() {
		it("Constructor with valid parameters", () => {
			const m1 = new OpenStreetCam();
			
			//Check attributes
			assert.equal(m1.name, "OpenStreetCam");
			assert.ok(/https:\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?/.test(m1.homepageUrl));
			assert.ok(/https:\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?/.test(m1.logoUrl));
		});
	});

	describe("requestPictures", function() {
		it("requestPictures with valid parameters", () => {
			const m1 = new OpenStreetCam();
			const bbox = new LatLngBounds(new LatLng(37.7642, -122.4604), new LatLng(37.7765, -122.4467));
			
			return m1.requestPictures(bbox)
			.then(pictures => {
				assert.ok(pictures.length > 0);
				
				const pic1 = pictures[0];
				assert.ok(pic1.author.length > 0);
				assert.equal(pic1.license, "CC By-SA 4.0");
				assert.ok(pic1.date > 0);
				assert.ok(pic1.pictureUrl.startsWith("https://storage"));
				assert.ok(pic1.detailsUrl.startsWith("https://openstreetcam.org/details/"));
				assert.equal(pic1.provider, "OpenStreetCam");
				assert.ok(bbox.contains(pic1.coordinates));
				assert.ok(pic1.direction === null || !isNaN(pic1.direction));
				assert.equal(pic1.osmTags.image, pic1.pictureUrl);

			})
			.catch(e => {
				console.log("Failed "+e);
				assert.ok(false);
			});
		}).timeout(REQUEST_TIMEOUT);

		it("requestPictures with valid parameters on empty area", () => {
			const m1 = new OpenStreetCam();
			const bbox = new LatLngBounds(new LatLng(-39.58687, -29.62719), new LatLng(-39.58498, -29.62503));

			return m1.requestPictures(bbox)
			.then(pictures => {
				assert.equal(pictures.length, 0);
			})
			.catch(e => {
				console.log(e);
				assert.ok(false);
			});
		}).timeout(REQUEST_TIMEOUT);

		it("requestPictures with valid parameters and options defined", () => {
			const m1 = new OpenStreetCam();
			const bbox = new LatLngBounds(new LatLng(40.724, -73.989), new LatLng(40.725, -73.988));
			
			return m1.requestPictures(bbox, { mindate: 1467377425000 })
			.then(pictures => {
				assert.ok(pictures.length > 0);
				
				const pic1 = pictures[0];
				assert.ok(pic1.author.length > 0);
				assert.equal(pic1.license, "CC By-SA 4.0");
				assert.ok(pic1.date >= 1467377425000);
				assert.ok(pic1.pictureUrl.startsWith("https://storage"));
				assert.ok(pic1.detailsUrl.startsWith("https://openstreetcam.org/details/"));
				assert.equal(pic1.provider, "OpenStreetCam");
				assert.ok(bbox.contains(pic1.coordinates));
				assert.ok(pic1.direction === null || !isNaN(pic1.direction));
			});
		}).timeout(REQUEST_TIMEOUT);
	});

	describe("requestSummary", function() {
		it("requestSummary with valid parameters", () => {
			const m1 = new OpenStreetCam();
			const bbox = new LatLngBounds(new LatLng(37.7642, -122.4604), new LatLng(37.7765, -122.4467));
			
			return m1.requestSummary(bbox)
			.then(stats => {
				assert.ok(stats.amount.length > 0);
				assert.ok(/^[e>][0-9]+$/.test(stats.amount));
				assert.ok(stats.last > 0);
				assert.equal(stats.bbox, bbox.toBBoxString());
			})
			.catch(e => {
				console.log(e);
				assert.ok(false);
			});
		}).timeout(REQUEST_TIMEOUT);

		it("requestSummary with valid parameters and options", () => {
			const m1 = new OpenStreetCam();
			const bbox = new LatLngBounds(new LatLng(40.724, -73.989), new LatLng(40.725, -73.988));
			
			return m1.requestSummary(bbox, { mindate: 1467377425000 })
			.then(stats => {
				assert.ok(stats.amount.length > 0);
				assert.ok(/^[e>][0-9]+$/.test(stats.amount));
				assert.ok(stats.last >= 1467377425000);
				assert.equal(stats.bbox, bbox.toBBoxString());
			})
			.catch(e => {
				console.log(e);
				assert.ok(false);
			});
		}).timeout(REQUEST_TIMEOUT);

		it("requestSummary over empty Paris in january 2017", () => {
			const m1 = new OpenStreetCam();
			const bbox = new LatLngBounds(new LatLng(48.84,2.305), new LatLng(48.845,2.31));
			
			return m1.requestSummary(bbox, { mindate: 1483228800000, maxdate: 1484870400000 })
			.then(stats => {
				assert.equal(stats.amount, "e0");
				assert.equal(stats.last, 0);
				assert.equal(stats.bbox, bbox.toBBoxString());
			})
			.catch(e => {
				console.log(e);
				assert.ok(false);
			});
		}).timeout(REQUEST_TIMEOUT);
	});
	
	describe("tagsToPictures", () => {
		it("works", () => {
			const m1 = new OpenStreetCam();
			const tags = { amenity: "bar", image: "https://storage7.openstreetcam.org/files/photo/2018/3/3/proc/1131761_dda80_44.jpg" };
			const result = m1.tagsToPictures(tags);
			
			assert.equal(result.length, 1);
			assert.equal(result[0], "https://storage7.openstreetcam.org/files/photo/2018/3/3/proc/1131761_dda80_44.jpg");
		});
		
		it("ignores invalid tag value", () => {
			const m1 = new OpenStreetCam();
			const tags = { amenity: "bar", image: "NOT A VALID IMAGE" };
			const result = m1.tagsToPictures(tags);
			
			assert.equal(result.length, 0);
		});
		
		it("handles multiple values", () => {
			const m1 = new OpenStreetCam();
			const tags = { amenity: "bar", image: "https://storage7.openstreetcam.org/files/photo/2018/3/3/proc/1131761_dda80_44.jpg;https://storage7.openstreetcam.org/files/photo/2018/3/3/proc/1131761_213f7_45.jpg" };
			const result = m1.tagsToPictures(tags);
			
			assert.equal(result.length, 2);
			assert.equal(result[0], "https://storage7.openstreetcam.org/files/photo/2018/3/3/proc/1131761_dda80_44.jpg");
			assert.equal(result[1], "https://storage7.openstreetcam.org/files/photo/2018/3/3/proc/1131761_213f7_45.jpg");
		});
	});
});
