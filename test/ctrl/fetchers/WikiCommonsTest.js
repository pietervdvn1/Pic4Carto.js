/*
 * Test script for ctrl/fetchers/WikiCommons.js
 */

global.XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
global.XMLHttpRequest.DONE = 4;

const assert = require('assert');
const LatLng = require("../../../src/model/LatLng");
const LatLngBounds = require("../../../src/model/LatLngBounds");
const WikiCommons = require("../../../src/ctrl/fetchers/WikiCommons");

const REQUEST_TIMEOUT = 30000;

describe("Ctrl > Fetchers > WikiCommons", function() {
	describe("Constructor", function() {
		it("Constructor with valid parameters", () => {
			const m1 = new WikiCommons();
			
			//Check attributes
			assert.equal(m1.name, "Wikimedia Commons");
			assert.ok(/(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?/.test(m1.homepageUrl));
			assert.ok(/(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?/.test(m1.logoUrl));
		});
	});

	describe("requestPictures", function() {
		it("requestPictures with valid parameters", () => {
			const m1 = new WikiCommons();
			const bbox = new LatLngBounds(new LatLng(48.85,2.2899999999999996), new LatLng(48.86,2.2999999999999994));
			
			return m1.requestPictures(bbox)
			.then(pictures => {
				assert.ok(pictures.length > 0);
				
				for(let i=0; i < pictures.length && i < 20; i++) {
					const pic = pictures[i];
					assert.ok(pic.author.length > 0);
					assert.ok(pic.date <= Date.now());
					assert.ok(pic.date > 0);
					assert.ok(pic.pictureUrl.startsWith("https://upload.wikimedia.org/wikipedia/commons/"));
					assert.ok(pic.detailsUrl.startsWith("https://commons.wikimedia.org/wiki/File:"));
					assert.equal(pic.provider, "Wikimedia Commons");
					assert.ok(bbox.contains(pic.coordinates));
					assert.equal(pic.direction, null);
					assert.ok(pic.osmTags.wikimedia_commons.startsWith("File:"));
					assert.ok(pic.thumbUrl.startsWith("https://upload.wikimedia.org/wikipedia/commons/"));
				}
			})
			.catch(e => {
				console.log(e);
				assert.ok(false);
			});
		}).timeout(REQUEST_TIMEOUT);

		it("requestPictures with valid parameters on empty area", () => {
			const m1 = new WikiCommons();
			const bbox = new LatLngBounds(new LatLng(31.410000000000004,-42.43000000000001), new LatLng(31.420000000000005,-42.42000000000001));
			
			return m1.requestPictures(bbox)
			.then(pictures => {
				assert.equal(pictures.length, 0);
			})
			.catch(e => {
				console.log(e);
				assert.ok(false);
			});
		}).timeout(REQUEST_TIMEOUT);

		it("requestPictures with valid parameters and options", () => {
			const m1 = new WikiCommons();
			const bbox = new LatLngBounds(new LatLng(48.85,2.2899999999999996), new LatLng(48.86,2.2999999999999994));
			
			return m1.requestPictures(bbox, { mindate: 1467377425000, maxdate: 1475326225000 })
			.then(pictures => {
				assert.ok(pictures.length > 0);
				
				for(let i=0; i < pictures.length && i < 20; i++) {
					const pic = pictures[i];
					assert.ok(pic.author.length > 0);
					assert.ok(pic.date <= 1475326225000);
					assert.ok(pic.date >= 1467377425000);
					assert.ok(pic.pictureUrl.startsWith("https://upload.wikimedia.org/wikipedia/commons/"));
					assert.ok(pic.detailsUrl.startsWith("https://commons.wikimedia.org/wiki/File:"));
					assert.equal(pic.provider, "Wikimedia Commons");
					assert.ok(bbox.contains(pic.coordinates));
					assert.equal(pic.direction, null);
				}
			})
			.catch(e => {
				console.log(e);
				assert.ok(false);
			});
		}).timeout(REQUEST_TIMEOUT);
	});

	describe("requestSummary", function() {
		it("requestSummary with valid parameters", () => {
			const m1 = new WikiCommons();
			const bbox = new LatLngBounds(new LatLng(48.85,2.2899999999999996), new LatLng(48.86,2.2999999999999994));
			
			return m1.requestSummary(bbox)
			.then(stats => {
				assert.ok(stats.amount.length > 0);
				assert.ok(/^[e>][0-9]+$/.test(stats.amount));
				assert.ok(stats.last > 0);
				assert.equal(stats.bbox, bbox.toBBoxString());
			})
			.catch(e => {
				console.log(e);
				assert.ok(false);
			});
		}).timeout(REQUEST_TIMEOUT);

		it("requestSummary with valid parameters and options", () => {
			const m1 = new WikiCommons();
			const bbox = new LatLngBounds(new LatLng(48.85,2.2899999999999996), new LatLng(48.86,2.2999999999999994));
			
			return m1.requestSummary(bbox, { mindate: 1467377425000, maxdate: 1475326225000 })
			.then(stats => {
				assert.ok(stats.amount.length > 0);
				assert.ok(/^[e>][0-9]+$/.test(stats.amount));
				assert.ok(stats.last >= 1467377425000);
				assert.ok(stats.last <= 1475326225000);
				assert.equal(stats.bbox, bbox.toBBoxString());
			})
			.catch(e => {
				console.log(e);
				assert.ok(false);
			});
		}).timeout(REQUEST_TIMEOUT);
	});
	
	describe("tagsToPictures", () => {
		it("works", () => {
			const m1 = new WikiCommons();
			const tags = { amenity: "bar", wikimedia_commons: "File:Nasir-al molk -1.jpg" };
			const result = m1.tagsToPictures(tags);
			
			assert.equal(result.length, 1);
			assert.equal(result[0], "https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/Nasir-al_molk_-1.jpg/1024px-Nasir-al_molk_-1.jpg");
		});
		
		it("ignores invalid tag value", () => {
			const m1 = new WikiCommons();
			const tags = { amenity: "bar", wikimedia_commons: "Not a wikimedia commons file" };
			const result = m1.tagsToPictures(tags);
			
			assert.equal(result.length, 0);
		});
		
		it("handles multiple values", () => {
			const m1 = new WikiCommons();
			const tags = { amenity: "bar", wikimedia_commons: "File:Nasir-al molk -1.jpg;File:BlueMarble-2001-2002.jpg" };
			const result = m1.tagsToPictures(tags);
			
			assert.equal(result.length, 2);
			assert.equal(result[0], "https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/Nasir-al_molk_-1.jpg/1024px-Nasir-al_molk_-1.jpg");
			assert.equal(result[1], "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1c/BlueMarble-2001-2002.jpg/1024px-BlueMarble-2001-2002.jpg");
		});
	});
});
