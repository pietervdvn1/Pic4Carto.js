/*
 * Test script for ctrl/fetchers/CSV.js
 */

global.XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
global.XMLHttpRequest.DONE = 4;

const assert = require('assert');
const express = require('express');
const LatLng = require("../../../src/model/LatLng");
const LatLngBounds = require("../../../src/model/LatLngBounds");
const CSV = require("../../../src/ctrl/fetchers/CSV");

const REQUEST_TIMEOUT = 30000;
const API_PORT = 3001;

const setupServer = () => {
	const app = express();
	app.use(express.static('test/assets'));
	return app.listen(API_PORT);
};

describe("Ctrl > Fetchers > CSV", function() {
	describe("Constructor", function() {
		it("Handles valid parameters", () => {
			const m1 = new CSV("http://localhost/data.csv");
			
			//Check attributes
			assert.equal(m1.csvURL, "http://localhost/data.csv");
			assert.equal(m1.name, "CSV Source");
			assert.equal(m1.homepageUrl, "");
			assert.equal(m1.logoUrl, "");
		});

		it("Handles missing parameters", () => {
			assert.throws(
				function() {
					const m1 = new CSV();
				},
				Error,
				"ctrl.fetchers.csv.invalidcsvurl"
			);
		});
		
		it("Handles optional parameters", () => {
			const m1 = new CSV("http://localhost/data.csv", {
				name: "Test of CSV",
				homepage: "http://test.net/csv",
				logo: "http://test.net/logo",
				license: "CC0",
				user: "Anon",
				bbox: new LatLngBounds(new LatLng(1,2), new LatLng(3,4))
			});
			
			//Check attributes
			assert.equal(m1.csvURL, "http://localhost/data.csv");
			assert.equal(m1.name, "Test of CSV");
			assert.equal(m1.homepageUrl, "http://test.net/csv");
			assert.equal(m1.logoUrl, "http://test.net/logo");
			assert.equal(m1.options.license, "CC0");
			assert.equal(m1.options.user, "Anon");
			assert.ok(m1.options.bbox.equals(new LatLngBounds(new LatLng(1,2), new LatLng(3,4))));
		});
	});

	describe("requestPictures", function() {
		it("with valid parameters", () => {
			const server = setupServer();
			
			const m1 = new CSV("http://localhost:"+API_PORT+"/csv_fetcher_1.csv");
			const bbox = new LatLngBounds(new LatLng(-0.005, -0.005), new LatLng(0.0015, 0.0015));
			
			return m1.requestPictures(bbox)
			.then(pictures => {
				server.close();
				
				assert.ok(pictures.length, 2);
				
				const pic1 = pictures[0];
				assert.equal(pic1.author, "User #1");
				assert.equal(pic1.license, "CC By-Sa 2.0");
				assert.equal(pic1.date, 1234000);
				assert.equal(pic1.pictureUrl, "http://localhost:"+API_PORT+"/images/001.jpg");
				assert.equal(pic1.detailsUrl, "http://localhost:"+API_PORT+"/details/001");
				assert.equal(pic1.provider, "CSV Source");
				assert.ok(bbox.contains(pic1.coordinates));
				assert.equal(pic1.direction, 152);
				assert.equal(pic1.osmTags.image, "http://localhost:"+API_PORT+"/images/001.jpg");
				
				const pic2 = pictures[1];
				assert.equal(pic2.author, "User #3");
				assert.equal(pic2.license, "CC0");
				assert.equal(pic2.date, 1236000);
				assert.equal(pic2.pictureUrl, "http://localhost:"+API_PORT+"/images/003.jpg");
				assert.equal(pic2.detailsUrl, "http://localhost:"+API_PORT+"/details/003");
				assert.equal(pic2.provider, "CSV Source");
				assert.ok(bbox.contains(pic2.coordinates));
				assert.equal(pic2.direction, 42);
				assert.equal(pic2.osmTags.image, "http://localhost:"+API_PORT+"/images/003.jpg");
			})
			.catch(e => {
				server.close();
				assert.fail(e);
			});
		}).timeout(REQUEST_TIMEOUT);

		it("with valid parameters on empty area", () => {
			const server = setupServer();
			
			const m1 = new CSV("http://localhost:"+API_PORT+"/csv_fetcher_1.csv");
			const bbox = new LatLngBounds(new LatLng(-1, -1), new LatLng(-0.5, -0.5));

			return m1.requestPictures(bbox)
			.then(pictures => {
				server.close();
				assert.equal(pictures.length, 0);
			})
			.catch(e => {
				server.close();
				assert.fail(e);
			});
		}).timeout(REQUEST_TIMEOUT);

		it("with valid parameters and date filtering", () => {
			const server = setupServer();
			
			const m1 = new CSV("http://localhost:"+API_PORT+"/csv_fetcher_1.csv");
			const bbox = new LatLngBounds(new LatLng(-1, -1), new LatLng(1, 1));
			
			return m1.requestPictures(bbox, { mindate: 0, maxdate: 1234000 })
			.then(pictures => {
				assert.ok(pictures.length > 0);
				
				for(const p of pictures) {
					assert.ok(p.date >= 0 && p.date <= 1234000);
				}
				
				server.close();
			})
			.catch(e => {
				server.close();
				assert.fail(e);
			});
		}).timeout(REQUEST_TIMEOUT);
		
		it("with valid parameters and use of default fetcher params", () => {
			const server = setupServer();
			
			const m1 = new CSV("http://localhost:"+API_PORT+"/csv_fetcher_2.csv", {
				license: "default license",
				user: "default user"
			});
			const bbox = new LatLngBounds(new LatLng(-1, -1), new LatLng(1, 1));
			
			return m1.requestPictures(bbox)
			.then(pictures => {
				assert.ok(pictures.length > 0);
				
				assert.equal(pictures[0].author, "default user");
				assert.equal(pictures[1].license, "default license");
				assert.equal(pictures[2].detailsUrl, pictures[2].pictureUrl);
				assert.equal(pictures[3].direction, null);
				
				server.close();
			})
			.catch(e => {
				server.close();
				assert.fail(e);
			});
		}).timeout(REQUEST_TIMEOUT);
		
		it("with valid parameters with data bbox defined", () => {
			const server = setupServer();
			
			const m1 = new CSV("http://localhost:"+API_PORT+"/csv_fetcher_1.csv", {
				bbox: new LatLngBounds(new LatLng(3,3), new LatLng(4,4))
			});
			const bbox = new LatLngBounds(new LatLng(-1, -1), new LatLng(-0.5, -0.5));

			return m1.requestPictures(bbox)
			.then(pictures => {
				server.close();
				assert.equal(pictures.length, 0);
			})
			.catch(e => {
				server.close();
				assert.fail(e);
			});
		}).timeout(REQUEST_TIMEOUT);
	});

	describe("requestSummary", function() {
		it("requestSummary with valid parameters", () => {
			const server = setupServer();
			
			const m1 = new CSV("http://localhost:"+API_PORT+"/csv_fetcher_1.csv");
			const bbox = new LatLngBounds(new LatLng(-0.005, -0.005), new LatLng(0.0015, 0.0015));
			
			return m1.requestSummary(bbox)
			.then(stats => {
				assert.ok(stats.amount.length > 0);
				assert.ok(/^[e>][0-9]+$/.test(stats.amount));
				assert.ok(stats.last > 0);
				assert.equal(stats.bbox, bbox.toBBoxString());
				
				server.close();
			})
			.catch(e => {
				server.close();
				assert.fail(e);
			});
		}).timeout(REQUEST_TIMEOUT);

		it("requestSummary with valid parameters and options", () => {
			const server = setupServer();
			
			const m1 = new CSV("http://localhost:"+API_PORT+"/csv_fetcher_1.csv");
			const bbox = new LatLngBounds(new LatLng(-0.005, -0.005), new LatLng(0.0015, 0.0015));
			
			return m1.requestSummary(bbox, { mindate: 0, maxdate: 1234000 })
			.then(stats => {
				assert.ok(stats.amount.length > 0);
				assert.ok(/^[e>][0-9]+$/.test(stats.amount));
				assert.ok(stats.last >= 0);
				assert.ok(stats.last <= 1234000);
				assert.equal(stats.bbox, bbox.toBBoxString());
				
				server.close();
			})
			.catch(e => {
				server.close();
				assert.fail(e);
			});
		}).timeout(REQUEST_TIMEOUT);
	});
});
